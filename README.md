## EMF OSGi

This project provides OSGi components to customize resource set configuration.

The project was inspired by Bryan Hunts eModelling:
https://github.com/BryanHunt/eModeling